package hydragyrum

import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.server.testing.TestApplicationCall
import io.ktor.server.testing.TestApplicationEngine
import io.ktor.server.testing.createTestEnvironment
import io.ktor.server.testing.handleRequest
import org.amshove.kluent.`should be equal to`
import org.amshove.kluent.`should be`
import org.amshove.kluent.`should equal to`
import org.amshove.kluent.`should equal`
import org.amshove.kluent.should
import org.amshove.kluent.shouldBeIn
import org.amshove.kluent.shouldNotBeNull
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.gherkin.Feature

object ApplicationSpec : Spek(
  {
    Feature("Application") {
      val engine by memoized { TestApplicationEngine(createTestEnvironment()) }
      engine.start(wait = false)
      engine.application.module(testing = true)

      with(engine) {
        Scenario("GET /hello") {
          lateinit var call: TestApplicationCall
          When("We call /hello") {
            call = handleRequest(HttpMethod.Get, "/hello")
          }
          Then("We get \"Hello, World!\"") {
            with(call) {
              response.content.shouldNotBeNull() `should be equal to` "Hello, World!"
              response.status() `should equal` HttpStatusCode.OK
            }
          }
        }

        Scenario("POST /hello") {
          lateinit var call: TestApplicationCall
          When("We call /hello") {
            call = handleRequest(HttpMethod.Post, "/hello")
          }
          Then("We don't handle the request") {
            with(call) {
              requestHandled `should be` false
            }
          }
        }
        
        Scenario("GET /") {
          lateinit var call: TestApplicationCall
          val expectedPage = """
                <!DOCTYPE html>
                <html lang="en">
                <head>
                  <meta charset="UTF-8">
                  <title>Test</title>
                </head>
                <body>
                  Test Index
                </body>
                </html>
              """.trimIndent()
          When("We call /") {
            call = handleRequest(HttpMethod.Get, "/")
          }
          Then("We get the home page") {
            with (call) {
              response.content.shouldNotBeNull() `should be equal to` expectedPage 
              response.status() `should equal` HttpStatusCode.OK
            }
          }
        }
      }
    }
  }
)